# EmoticonAdjustable

## 介绍

 CH549教程，使用ADC制作动态可变表情😐

## 详情

教程链接：https://verimake.com/d/167-51adc-ch549  
开发板资料：https://gitee.com/verimaker/vm-wch-51-evb-ch549evb  
其它教程：https://verimake.com/d/19-ch549-51  