# 说明

目录

- [说明](#说明)
	- [基于本项目建立新项目](#基于本项目建立新项目)

## 基于本项目建立新项目

本项目依赖的库有一部分在 `../lib/` 文件夹下（即上一级目录中，与本项目文件夹并排的 `lib` 文件夹），
需保证 `lib` 与本项目位于同一目录中。  
如需基于本项目建立新项目：

1. 若新项目与本项目在同一目录中，直接复制本项目改名即可；  
   否则应同时复制本项目和 `lib` 文件夹到新的位置。
2. 修改编译目标名称：在本项目根目录下的 `makefile` 中，修改 `TARGET_NAME` 的值即可

```makefile
#编译目标名称
TARGET_NAME = 新的名称
```

3. 添加包含路径与源文件：  
   在本项目根目录下的 `makefile` 中，修改 `C_INCLUDES` 添加新的包含路径。  
   修改 `C_SOURCES` 添加新的编译文件，所有参与编译的 源文件都要添加到 `C_SOURCES` 中

```makefile
#参与编译的源文件
C_SOURCES =  \
usr/main.c \
source/CH549_LCD.c \
../lib/CH549_DEBUG.c \
../lib/CH549_SPI.c \
../lib/CH549_ADC.c \

#头文件路径
C_INCLUDES =  \
-Iinc \
-Isource \
-I../lib/inc \
```
